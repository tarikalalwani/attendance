@extends ('layouts.newdashboard')
@section('page_heading','User Update')
@section('section')

    {{ Form::model($user,['method' => 'PUT', 'route' => ['users.update', $user['id']],'class'=>'form-horizontal','id'=>'user']) }}

    <div class="form-group @if ($errors->has('name')) has-error @endif">
        <div class="col-sm-1">
            {{ Form::label('name', 'Name:') }}
        </div>

        <div class="col-sm-6">
            {{ Form::text('name',isset($user['name'])? $user['name'] : null, ['class' => 'form-control','required'=>'required']) }}
            @if ($errors->has('name')) <p class="error-block">{{ $errors->first('name') }}</p> @endif
        </div>
    </div>

    <div class="form-group @if ($errors->has('email')) has-error @endif">
        <div class="col-sm-1">
            {{ Form::label('email', 'Email:') }}
        </div>
        <div class="col-sm-6">
            {{ Form::email('email', isset($user['email'])? $user['email'] : null, ['class' => 'form-control','required'=>'required', 'autocomplete' => 'off']) }}
            @if ($errors->has('email')) <p class="error-block">{{ $errors->first('email') }}</p> @endif
        </div>
    </div>

    <div class="form-group @if ($errors->has('isAdmin')) has-error @endif">

        <div class="form-group col-sm-12">
            <div class="col-sm-2 col-xs-5">
                {{ Form::label('isAdmin','Is Admin:', array('class' => 'control-label label-text-align-left')) }}
            </div>

            <div class="col-sm-6 col-xs-4">
                {{ Form::checkbox('isAdmin','1',$user['is_admin'], ['class' => 'form-control']) }}
            </div>
        </div>

        @if ($errors->has('isAdmin')) <p class="error-block">{{ $errors->first('isAdmin') }}</p> @endif
    </div>

    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-6">
            {{ Form::submit(isset($user) ? 'Update' : 'Save', ['class' => 'btn btn-default approve-btn']) }}
        </div>
    </div>
    {{ Form::close() }}

@stop

@section('script')
    <script type="text/javascript">

    </script>
@stop