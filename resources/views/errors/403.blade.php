@extends ('layouts.newdashboard')
@section('page_heading','403 - Access Denied')
@section('section')

	<div class="error-page">
	    <div class="error-content">

	        <p>
				<i class="fa fa-warning text-yellow"></i>You don't have permission to access this page!
	        </p>
	    </div><!-- /.error-content -->
	</div><!-- /.error-page -->

@endsection
