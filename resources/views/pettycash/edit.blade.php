@extends($layout)

@section('title')
	Edit Template
@stop

@section('content-header')      
    <h1>
        Edit Template 
    </h1>
@endsection

@section('content')
    <div>
        @include('admin::template.form')
    </div>
@endsection