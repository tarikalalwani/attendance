@extends($layout)

@section('title')
	Templates
@stop

@section('content-header')
      <div class="pageheader custom-pageheader">
        <div class="media">
          <div class="media-body">
            <h4>TEMPLATES</h4>
            <div class="add-template"> <a href="/template/add" class="row-add">
              <p>ADD TEMPLATE</p>
              <span class="glyphicon glyphicon-plus-sign"></span></a> 
            </div>
          </div>
        </div>
        <!-- media --> 
      </div>
@stop

@section('content')
<div class="contentpanel">
	<div class="panel panel-primary-head">
	  <table id="" class="table table-striped table-bordered responsive drag-table">
		<thead>
		  <tr>
		    <th></th>
	        <th>Template Name</th>
	        <th>Task</th>
	        
		  </tr>
		</thead>
	<tbody>
		@if(sizeof($list)>0)
			@foreach($list as $key=>$value)
			<tr>
				<td class="template-list-action"><a href="edit/{!! $value['id'] !!}" class="row-edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
				<td><a href="/templateactivities/list/{!! $value['id'] !!}" >{!! $value['name'] !!} </td></a>
				<td>{!! $value['tasks'] !!}</td>
			</tr>
			@endforeach
		@else
			<tr ><td colspan="2">No Template Added</td></tr>
		@endif
	</tbody>
</table>
</div>
</div>
@endsection
