@extends ('layouts.newdashboard')
@section('page_heading','Change Password')
@section('section')

    @if(Session::has('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>
            {{ Session::get('success') }}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>
            {{ Session::get('error') }}
        </div>
    @endif

    <div class="body bg-gray bg-white">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{ Form::open(['method' => 'POST', 'route' => 'users.updatepassword','class'=>'form-horizontal','role'=>'form','id'=>'changepassword']) }}

                    <div class="form-group">
                        {{ Form::label('password', 'New password *', ['class' => 'col-sm-3 control-label']) }}
                        <div class="col-sm-9">
                            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'autocomplete' => 'off']) }}
                            <small class="text-danger">{{ $errors->first('password') }}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('password_confirmation', 'Confirm your new password *', ['class' => 'col-sm-3 control-label']) }}
                        <div class="col-sm-9">
                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'autocomplete' => 'off']) }}
                            <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                        </div>
                    </div>

                    <div class="btn-group pull-right">
                        {{ Form::submit("Update", ['class' => 'btn btn-primary approve-btn']) }}
                    </div>

                    {{ Form::close() }}


                </div>
            </div>
        </div>
    </div>

@stop
