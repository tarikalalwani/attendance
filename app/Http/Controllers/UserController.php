<?php namespace App\Http\Controllers;

use App\Date;
use App\User;
use App\UsersLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\RoleUser;

class UserController extends Controller {

    protected $users;

    public function index(){
        $isAdmin= User::isAdmin(\Auth::user()->id);
        if(!$isAdmin){
            return view('errors.403');
        }

        $list = array();
        $i = 0;

        $users = \DB::table('users')->orderBy('name','asc')->get();
        foreach ($users as $key=>$value){
            $list[$value->id]['name'] = $value->name;
            $list[$value->id]['email'] = $value->email;
            $list[$value->id]['id'] = $value->id;
        }
        //print_r($list);exit;
        return view('users.index',array("list"=>$list));
    }

    public function add(){

    }

    public function edit($id){

        $isAdmin= User::isAdmin(\Auth::user()->id);
        if(!$isAdmin){
            return view('errors.403');
        }

        $users = \DB::table('users')->where('id',$id)->first();
        $is_admin = \DB::table('role_user')->where('user_id',$id)->where('role_id',1)->first();

        $user = array();
        if(sizeof($users)>0){
            $user['id'] = $users->id;
            $user['name'] = $users->name;
            $user['email'] = $users->email;
            $user['is_admin'] = sizeof($is_admin);
        }

        return view('users.update', compact('user'));
    }
    public function update($id){
        $is_admin = User::isAdmin($id);
        
        $data = Input::all();

        if(isset($data)){
            $user = User::findOrFail($id);

            if(isset($data['name'])){
                $user->name = $data['name'];
            }
            if(isset($data['email'])){
                $user->email = $data['email'];
            }

            $userUpdated = $user->save();

            $isAdmin = Input::get('isAdmin',NULL);
            RoleUser::where('user_id',$id)->delete();
            if($isAdmin){
                $role_user = new RoleUser();
                $role_user->user_id = $id;
                $role_user->role_id = '1';
                $roleUpdated = $role_user->save();
            }
            else{
                $role_user = new RoleUser();
                $role_user->user_id = $id;
                $role_user->role_id = '2';
                $roleUpdated = $role_user->save();
            }
            if(isset($userUpdated))
            {
                return redirect('users')->with('success','User Updated Successfully');
            }
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $isAdmin= User::isAdmin(\Auth::user()->id);
        if(!$isAdmin){
            return view('errors.403');
        }

        User::where('id',$id)->delete();
        return redirect('users')->with('success','User Deleted Successfully');
    }

    public function changepassword(){

        return view('users.changepassword');
    }

    public function updatepassword(){

        $user  = \Auth::user();

        $data = Input::all();

        $rules = array(
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8'
        );

        // Create a new validator instance.
        $validator = \Validator::make($data, $rules);

        $user->password = \Hash::make($data['password']);
        $user->save();

        if($validator->fails()){
            return redirect('changepassword')->withInput(Input::all())->withErrors($validator->errors());
        }

        return redirect('changepassword')->with('success','Password Change Successfully.');
    }
}