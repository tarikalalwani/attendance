<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array ('uses' => 'Auth\AuthController@getLogin'));
Route::get('/auth/login', array ('uses' => 'Auth\AuthController@getLogin'));
Route::post('/auth/login',array('uses' => 'Auth\AuthController@postLogin'));
Route::get('/logout', array ('uses' => 'Auth\AuthController@getLogout'));

Route::get('/register', function()
{
	return View::make('auth.register');
});
Route::post('/auth/register',array('uses' => 'Auth\AuthController@postRegister'));

Route::any('/attendance', array (
	'middleware' => 'auth',
	'uses' => 'DashboardController@attendance'
));
Route::any('/home', array (
	'middleware' => 'auth',
	'uses' => 'DashboardController@attendance'
));
// Users
Route::get('/users', array (
	'middleware' => 'auth',
	'as' => 'users.index',
	'uses' => 'UserController@index'
));
Route::get('/users/edit/{id}', array (
	'middleware' => 'auth',
	'as' => 'users.edit',
	'uses' => 'UserController@edit'
));
Route::put('/users/edit/{id}', array (
	'middleware' => 'auth',
	'as' => 'users.update',
	'uses' => 'UserController@update'
));
Route::delete ('users/{id}/destroy', array (
	'middleware' => 'auth',
	'as' => 'users.destroy',
	'uses' => 'UserController@destroy'
));
Route::get ('changepassword', array (
	'middleware' => 'auth',
	'as' => 'users.changepassword',
	'uses' => 'UserController@changepassword'
));
Route::post ('updatepassword', array (
	'middleware' => 'auth',
	'as' => 'users.updatepassword',
	'uses' => 'UserController@updatepassword'
));

// PettyCash
Route::get('/pettycash/create', array (
        'middleware' => 'auth',
        'as' => 'pettycash.create',
        'uses' => 'PettycashController@create'
));
Route::post ('pettycash/add', array (
        'middleware' => 'auth',
        'as' => 'pettycash.add',
        'uses' => '\App\Http\Controllers\PettycashController@add'
));

Route::get('/icons', function()
{
	return View::make('icons');
});

Route::get('/charts', function()
{
	return View::make('mcharts');
});

Route::get('/tables', function()
{
	return View::make('table');
});

Route::get('/forms', function()
{
	return View::make('form');
});

Route::get('/grid', function()
{
	return View::make('grid');
});

Route::get('/buttons', function()
{
	return View::make('buttons');
});


Route::get('/icons', function()
{
	return View::make('icons');
});

Route::get('/panels', function()
{
	return View::make('panel');
});

Route::get('/typography', function()
{
	return View::make('typography');
});

Route::get('/notifications', function()
{
	return View::make('notifications');
});

Route::get('/blank', function()
{
	return View::make('blank');
});



Route::get('/documentation', function()
{
	return View::make('documentation');
});
