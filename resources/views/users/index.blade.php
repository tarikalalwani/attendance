@extends ('layouts.newdashboard')
@section('page_heading','Users')
@section('section')
    @if(Session::has('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>
            {{ Session::get('success') }}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>
            {{ Session::get('error') }}
        </div>

    @endif

    <table class="table table-condensed table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $key=>$value)
        <tr>
            <td>{{ $value['name'] }}</td>
            <td>{{ $value['email'] }}</td>
            <td><a href="/users/edit/{{$key}}"><i class="fa fa-edit fa-fw"></i></a> | @include('partials.modal', ['data' => $value, 'name' => 'users'])</td>
        </tr>
        @endforeach
        </tbody>
    </table>
@stop

@section('script')
    <script type="text/javascript">

    </script>
@stop