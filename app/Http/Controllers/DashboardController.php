<?php namespace App\Http\Controllers;

use App\Date;
use App\User;
use App\UsersLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function attendance(){
        $loggedin_userid = \Auth::user()->id;

        $isAdmin= User::isAdmin($loggedin_userid);

        $list=array();

        if(isset($_POST['month']) && $_POST['month']!=''){
            $month = $_POST['month'];
        }
        else{
            $month = date("n");
        }
        if(isset($_POST['year']) && $_POST['year']!=''){
            $year = $_POST['year'];
        }
        else{
            $year = date("Y");
        }

		$month_array =array();
		for ($m=1; $m<=12; $m++) {
			$month_array[$m] = date('M', mktime(0,0,0,$m));
		}
		
		$year_array = array();
		$year_array[2016] = 2016;
        $year_array[2017] = 2017;
        $year_array[2018] = 2018;
        
        if($isAdmin){
            $user_response = \DB::select("select users.id ,name from users ");
        }
        else{
            $user_response = \DB::select("select users.id ,name from users where users.id = $loggedin_userid ");
        }

        if(sizeof($user_response)>0){
            foreach ($user_response as $key => $value) {
                $list[$value->name]= "";
            }
        }

        for($d=1; $d<=31; $d++)
        {
            $time=mktime(12, 0, 0, $month, $d, $year);
            foreach ($list as $key => $value) {
                if (date('m', $time)==$month)
                    $list[$key][date('j S', $time)]['login']='';
                    $list[$key][date('j S', $time)]['logout']='';
					$list[$key][date('j S', $time)]['total']='';
            }
        }

        if($isAdmin){
            $response = \DB::select("select users.id ,name ,date ,min(time) as login , max(time) as logout from users_log
                        join users on users.id = users_log.user_id where month(date)= $month and year(date)=$year group by date ,users.id");
        }
        else{
            $response = \DB::select("select users.id ,name ,date ,min(time) as login , max(time) as logout from users_log
                        join users on users.id = users_log.user_id where month(date)= $month and year(date)=$year and users.id = $loggedin_userid group by date ,users.id");
        }


        $date = new Date();
        if(sizeof($response)>0){
            foreach ($response as $key => $value) {
                $login_time = $date->converttime($value->login);
                $logout_time = $date->converttime($value->logout);
                $list[$value->name][date("j S",strtotime($value->date))]['login'] = date("h:i A",$login_time);
                $list[$value->name][date("j S",strtotime($value->date))]['logout'] = date("h:i A",$logout_time);

                $total = ($logout_time - $login_time) / 60;

                $list[$value->name][date("j S",strtotime($value->date))]['total'] = date('H:i', mktime(0,$total));
            }
        }

        return view('attendance',array("list"=>$list,"month_list"=>$month_array,"year_list"=>$year_array,"month"=>$month,"year"=>$year));
    }
}