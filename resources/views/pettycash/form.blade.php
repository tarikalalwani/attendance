 @if( $action == 'edit')
    {!! Form::model($template, ['method' => 'PUT', 'files' => true, 'route' => ['admin.template.update', $template['id']] ,'class'=>'form-horizontal']) !!}
@else
    {!! Form::open(['route' => 'pettycash.add','class'=>'form-horizontal','method'=>'POST']) !!}
@endif


    <div class="form-group @if ($errors->has('name')) has-error @endif">
        <div class="col-sm-1"> {!! Form::label('title-name','Name', array('class' => 'control-label')) !!}</div>

        <div class="col-sm-6">
            {!! Form::text('name', null,array('class' => 'form-control','id' => 'name','placeholder'=>'Name')) !!}
            @if ($errors->has('name')) <p class="error-block">{!! $errors->first('name') !!}</p> @endif
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-6">
        @if( $action == 'edit')
            <input type="submit" value="Update" class="btn btn-default approve-btn" style="margin-right:15px;">
            
            <input type="button" value="Delete" onclick="deletetemplate({!! $template['id'] !!})" class="btn btn-default reject-btn">
        @else
            <input type="submit" value ="Add" class="btn btn-default approve-btn">
        @endif
            </input> 
        </div>
    </div>

{!! Form::close() !!}

@section('script')
    <script type="text/javascript">
    </script>
@endsection